//
//  UserAPICall.swift
//  FastHub
//
//  Created by Urvi Koladiya on 12/07/19.
//  Copyright © 2019 Urvi Koladiya. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    func loginWith(username: String, password: String, successBlock: @escaping (User?) -> (), ErrorBlock: @escaping (Bool) -> ()) {
        
        // MARK: - Request to Authenticate User
        
        let loginData = String(format: "%@:%@", username,password).data(using: String.Encoding.utf8)
        let base64LoginData = loginData?.base64EncodedString()
        let headers : HTTPHeaders = ["Authorization" : "Basic \(base64LoginData!)","Accept": "application/json", "Content-Type": "application/json"]
        
        // MARK: - decode authenticated user's Data
        
        AF.request("https://api.github.com/user", method: .get, encoding: URLEncoding.default, headers: headers).responseData { response in
            switch response.result {
            case .success(let json):
                let users = try? JSONDecoder().decode(User.self, from: json)
                
                if users != nil {
                    successBlock(users!)
                    UserDefaults.standard.set(json, forKey: "users")
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    UserDefaults.standard.synchronize()
                }else {
                    let Error = true
                    ErrorBlock(Error)
                }
            case .failure(let error):
                print(error)
                let Error = true
                ErrorBlock(Error)
            }
        }
    }
}

struct loginErrorAlert {
    static func showAlert(on vc: UIViewController) {
        let alert = UIAlertController(title: "Login Error", message: "Authentication Failed!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: nil))
        vc.present(alert, animated: true)
    }
}
