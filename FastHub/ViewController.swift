//
//  ViewController.swift
//  FastHub
//
//  Created by Urvi Koladiya on 11/07/19.
//  Copyright © 2019 Urvi Koladiya. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import FontAwesome_swift

class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtusername: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
  
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var signInLbl: UILabel!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var txtRequiedfield: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtusername.delegate = self
        txtpassword.delegate = self
        loginViewLayOutSetting()
        loaderPropertySetting()
        self.hideKeyboardTappedAround()
        setupKeyboardNotifications()
        TextFieldTappedEvent()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func TextFieldTappedEvent() {
        txtusername.addTarget(self, action: #selector(myTargetFunction), for: .touchUpInside)
        txtpassword.addTarget(self, action: #selector(myTargetFunction), for: .touchUpInside)
    }
    @objc func myTargetFunction(textField: UITextField) {
        loginbtn.isEnabled = true
    }
    
    // MARK: - keyBoard SetUp Methods
    private func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIControl.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIControl.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        let bottomInset = keyboardSize.height * 1.5
        
        scrollView.contentInset.bottom = bottomInset
        scrollView.scrollIndicatorInsets.bottom = bottomInset
        loginbtn.isEnabled = true
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
    
    // MARK: - Login Button Action
    @IBOutlet weak var loginbtn: UIButton!
    @IBAction func loginButton(_ sender: Any) {
        
        if txtpassword.text == "" && txtusername.text == ""{
            txtRequiedfield.text = "Required Username & Password"
        }
        else if txtusername.text == "" {
            txtRequiedfield.text = "Required Username"
        }
        else if txtpassword.text == ""{
             txtRequiedfield.text = "Required Password"
        }
        else {
            activity.isHidden = false
            activity.startAnimating()
        
            self.loginbtn.isEnabled = false
            APIManager().loginWith(username: txtusername.text!, password: txtpassword.text!, successBlock: { Users in
                
                let tabbar = self.storyboard?.instantiateViewController(identifier: "tabbar") as! UITabBarController
                UIApplication.shared.keyWindow?.rootViewController = tabbar
                self.loginbtn.isEnabled = true
                self.activity.isHidden = true
                self.activity.stopAnimating()
               
            }) { Error in
                loginErrorAlert.showAlert(on: self)
                self.loginbtn.isEnabled = true
                self.activity.isHidden = true
                self.activity.stopAnimating()
            }
            txtRequiedfield.text = ""
            
        }
        
    }
    
    // MARK: - set loginView Appearance
    func loginViewLayOutSetting() {
        loginView.layer.cornerRadius = 5
        loginView.layer.shadowColor = UIColor.black.cgColor
        loginView.layer.shadowOpacity = 0.7
        loginbtn.layer.cornerRadius = 5
        loginView.clipsToBounds = true
    }
    
    // MARK: - set ActivityIndegator property
    func loaderPropertySetting() {
       activity.isHidden = true
    }
}

// MARK: - dismiss KeyBoard by Tap on Return key
extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Dismiss KeyBoard by Tapping Around LoginView
extension UIViewController {
    func hideKeyboardTappedAround() {
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
